import { Injectable } from '@nestjs/common';
//Clase tipo servicio
@Injectable()
export class AppService {
  saludar(nombre: string): string {
    return ('Hello ' + nombre);
  }
}
