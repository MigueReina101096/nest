import { Get, Controller, Post, Put, Patch, Delete, Param, HttpService, UseInterceptors, FileInterceptor, UploadedFile, FilesInterceptor, UploadedFiles, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { ManticoreLoggerService } from 'logger/logger.service';
import { urlPaola } from 'paola-server';
import { map } from 'rxjs/operators';

@Controller('Estudiante')

export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly _manticoreLoggerService: ManticoreLoggerService,
    private readonly _httpService: HttpService) { }

  @Get('Hola')
  saludo() {
    return '<h1>' + this.appService.saludar('Miguel') + '</h1>';
  }
  @Get('Hola/:nombre/:apellido/:correo')
  holaParametroRuta(
    @Param('nombre')
    nombreParamtero: string,
    @Param('apellido')
    apellidoParametro: string,
    @Param('correo')
    correoParametro: string) {
    return this.appService.saludar(`${nombreParamtero} ${apellidoParametro} tu correo es ${correoParametro}`);
  }
  @Get('momazo')
  mostrarMomazo() {
    // TODO: Consultar momazo 192.168.100.38
    const ruta = '/';
    const url = urlPaola + ruta;
    this._manticoreLoggerService.log(url, '');
    const hartademenciaMomazo$ = this._httpService.get(url);
    this._manticoreLoggerService.log(`${hartademenciaMomazo$}`, '');
    return hartademenciaMomazo$
      .pipe(
        map((valorRespuestaHttp) => {
          const image = valorRespuestaHttp.data;
          // console.log(valorRespuestaHttp);
          return image;
        }),
      );
  }
  @Post('subirArchivo')
  @UseInterceptors(FilesInterceptor('test'))
  subirArchivo(@UploadedFiles() test1) {
    console.log(test1);
    return ('Se subio el archivo');
  }
  // /home/cris/Descargas
  // Calificaciones finales (1).xlsx

  @Get('descargarArchivo')
  descargarArchivo(@Res() res) {
    const path = '/home/cris/Escritorio/nest/backend-estudiante/upload/06f2c85eb33c0e5e6142c3e2a39c9cd7';
    const nombreArchivo = '06f2c85eb33c0e5e6142c3e2a39c9cd7';
    res.download(path, nombreArchivo);
  }
}
