import { Validator } from 'class-validator';
const validator = new Validator();
export class EstudiantDto {
    nombre: string;
    codigo: string;
}

validator.isAlpha('nombre');
validator.isAlphanumeric('codigo');