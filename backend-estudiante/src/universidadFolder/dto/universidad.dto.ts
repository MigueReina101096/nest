import { IsInt, IsOptional, IsAlpha, IsNumber, IsEmail, IsAlphanumeric, IsDateString } from 'class-validator';
export class UniversidadDto {
    @IsInt()
    @IsOptional()
    id?: number;
    @IsAlpha()
    nombre: string;
    @IsNumber()
    telefono: number;
    @IsEmail()
    correo: string;
    @IsAlphanumeric()
    direccion: string;
    @IsNumber()
    numeroFacultades: number;
    @IsNumber()
    numeroEstudiantes: number;
    @IsNumber()
    numeroEmpleados: number;
    @IsDateString()
    fechaFundacion: Date;
}