import { Injectable, createRouteParamDecorator } from '@nestjs/common';
import { UniversidadDto } from '../universidadFolder/dto/universidad.dto';
import { EPROTONOSUPPORT } from 'constants';
@Injectable()
export class UniversidadServicie {
    private recnum = 1;
    private readonly _universidades: UniversidadDto[] = [
        {
            id: 1,
            nombre: 'Escuela Politecnica Nacional',
            telefono: 2065952,
            correo: 'escuela.politecnica@epn.edu.ec',
            direccion: 'Barrio la Vicentina',
            numeroFacultades: 20,
            numeroEstudiantes: 40000,
            numeroEmpleados: 20000,
            fechaFundacion: new Date(),
        }
    ]

    buscarTodos() {
        return this._universidades;
    }
}