import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { ParseIntPipe, UsePipes } from '@nestjs/common';
import { EstudianteService } from './estudiante.service';
import { EstudiantDto } from './dto/estudiante.dto';
import { EstudianteCreatePipe } from './estudiante.create.pipe';


@Resolver('Estudiante')
export class EstudianteResolver {

    constructor(private readonly estudianteService: EstudianteService) {

    }

    @Query('obtenerPorId')
    obtenerPorId(
        @Args('id', ParseIntPipe) id: number
    ): EstudiantDto {
        return this.estudianteService.buscarPorId(id);
    }

    @Query('traerTodos')
    traerTodos(): EstudiantDto[] {
        return this.estudianteService.buscarTodos();
    }

    @Mutation('crearEstudiante')
    // @UsePipes(new EstudianteCreatePipe())
    crearEstudiante(
        @Args() estudianteDto: any
    ): EstudiantDto {
        return this.estudianteService.crearEstudiante(estudianteDto.estudiante);
    }

}