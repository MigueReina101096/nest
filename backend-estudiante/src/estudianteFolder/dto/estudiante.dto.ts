import { IsDateString, IsInt, IsOptional, IsAlpha, IsAlphanumeric, IsEmail, IsNumber, IsBoolean } from 'class-validator';
export class EstudiantDto {
    @IsInt()
    @IsOptional()
    id?: number;
    @IsAlpha()
    nombre: string;
    @IsAlpha()
    apellido: string;
    @IsAlphanumeric()
    cedula: string;
    @IsInt()
    edad: number;
    @IsBoolean()
    esEgresado: boolean;
    @IsDateString()
    fechaIngreso: Date;
}