import { Module } from '@nestjs/common';
import { EstudianteController } from './estudiante.controller';
import { EstudianteService } from './estudiante.service';
import { EstudianteResolver } from './estudiante.resolver';

@Module({
    imports: [],
    controllers: [EstudianteController],
    providers: [EstudianteService,EstudianteResolver],
})

export class EstudianteModule { }