import { Injectable } from '@nestjs/common';
import { EstudiantDto } from 'estudianteFolder/dto/estudiante.dto';

@Injectable()
export class EstudianteService {

  private recnum = 1;
  private readonly _estudiantes: EstudiantDto[] = [
    {
      id: 1,
      nombre: 'Miguel',
      apellido: 'Reina',
      cedula: '1751680974',
      edad: 22,
      esEgresado: false,
      fechaIngreso: new Date(),
    },
  ];

  buscarTodos() {
    return this._estudiantes;
  }

  buscarPorId(id: number): EstudiantDto {
    return this._estudiantes.find(estudiante => estudiante.id == id);
  }

  crearEstudiante(nuevoEstudiante: EstudiantDto): EstudiantDto {
    this.recnum += 1;
    nuevoEstudiante.id = this.recnum;
    this._estudiantes.push(nuevoEstudiante);
    return nuevoEstudiante;
  }

// editar(id: number, datosModificados: EstudianteEditarDto): EstudianteEditarDto{
    // metodoBuscarUsuarioPorId
    // actualizarNuevaData
//   elemento[indice]=nuevaData;
  //  return usuarioEditado;
  //}

  //eliminar(id: number): EstudiantDto {
  //  return this._estudiantes.splice(estudiante => estudiante.id == id);
 // }
}
