import { EstudianteCreatePipe } from './estudiante.create.pipe';
import { Get, Controller, Post, Body, Put, Patch, Delete, Param, UsePipes } from '@nestjs/common';
import { EstudianteService } from './estudiante.service';
import { EstudiantDto } from 'estudianteFolder/dto/estudiante.dto';

@Controller('estudiante')
export class EstudianteController {
  constructor(private readonly estudianteService: EstudianteService) { }

  @Get()
  buscarTodos() {
    return this.estudianteService.buscarTodos();
  }

  @Get(':id')
  obtenerPorId(
    @Param('id') idEstudiante,
  ) {
    return this.estudianteService.buscarPorId(idEstudiante);
  }

  @Post()
  @UsePipes(new EstudianteCreatePipe())
  crearEstudiante(
    @Body() estudianteDto: EstudiantDto,
  ) {
    return this.estudianteService.crearEstudiante(estudianteDto);
  }

}
