import { EstudianteModule } from './estudianteFolder/estudiante.module';
import { Module, HttpModule, MulterModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import * as GraphQLJSON from 'graphql-type-json';
import { LoggerModule } from 'logger/logger.module';

console.log(__dirname);

@Module({
  imports: [EstudianteModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      resolvers: { JSON: GraphQLJSON },
      }),
    HttpModule,
    LoggerModule,
    MulterModule.register({
      dest: __dirname+'/../upload',
    }),
    ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
